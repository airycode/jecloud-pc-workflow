import BaseNode from '../base-node';
import { isNotEmpty } from '@jecloud/utils';
import NodeFormControl from '../attrs/node-form-control';

export default class EndNode extends BaseNode {
  constructor(options) {
    super(options);
    /**
     * 基础配置
     */
    this.properties = {
      //id，不会重复的唯一id
      resourceId: isNotEmpty(options.properties) ? options.properties.resourceId : 'end',
      //名称
      name: isNotEmpty(options.properties) ? options.properties.name : '',
      //绑定表单名称
      formSchemeName:
        isNotEmpty(options.properties) && isNotEmpty(options.properties.formSchemeName)
          ? options.properties.formSchemeName
          : '',
      //绑定表单id
      formSchemeId:
        isNotEmpty(options.properties) && isNotEmpty(options.properties.formSchemeId)
          ? options.properties.formSchemeId
          : '',
    };
    /**
     * 表单控制
     */
    this.formConfig = new NodeFormControl(options.formConfig || {});
  }
}
