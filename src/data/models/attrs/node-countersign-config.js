export default class NodecountersignConfig {
  constructor(options) {
    /**
     * 规则类型
     */
    this.counterSignPassType = options.counterSignPassType || 'PASS_PERSENT';

    /**
     * 通过比例
     */
    this.amount = options.amount || '100';

    /**
     * 负责人id
     */
    this.oneBallotUserId = options.oneBallotUserId || '';

    /**
     * 负责人name
     */
    this.oneBallotUserName = options.oneBallotUserName || '';

    /**
     * 全部投票
     */
    this.voteAll = options.voteAll || '0';

    /**
     * 顺序审批
     */
    this.sequential = options.sequential || '0';

    /**
     * 运行时调整
     */
    this.runtimeTuning = options.runtimeTuning || '0';

    /**
     * 运行前调整
     */
    this.adjustBeforeRunning = options.adjustBeforeRunning || '0';

    /**
     * 取消全选
     */
    this.deselectAll = options.deselectAll || '0';
  }
}
