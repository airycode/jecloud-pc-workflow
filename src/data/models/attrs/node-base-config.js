export default class NodeBaseConfig {
  constructor(options, nodeType) {
    /**
     * 类型
     * 任务节点和判断节点特有的属性
     */
    if (['task', 'decision'].indexOf(nodeType) != -1) {
      this.categorydefinition = this.getCategorydefinition(nodeType);
    }

    /**
     * 节点类型
     */
    this.resourceId == options.resourceId || '';

    /**
     * 节点名称
     */
    this.name = options.name || '';

    /**
     * 绑定表单名称
     */
    this.formSchemeName = options.formSchemeName || '';

    /**
     * 绑定表单id
     */
    this.formSchemeId = options.formSchemeId || '';

    /**
     * 列表同步
     */
    this.listSynchronization = options.listSynchronization || '0';

    /**
     * 不可取回
     */
    this.retrieve = options.retrieve || '0';

    /**
     * 可催办
     */
    this.urge = options.urge || '0';

    /**
     * 可作废
     */
    this.invalid = options.invalid || '0';

    /**
     * 可转办
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.transfer = options.transfer || '0';
    }

    /**
     * 可委托
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.delegate = options.delegate || '0';
    }

    /**
     * 表单可编辑
     */
    this.formEditable = options.formEditable || '0';

    /**
     * 消息不提醒
     */
    this.remind = options.remind || '0';

    /**
     * 不简易审批
     */
    this.simpleApproval = options.simpleApproval || '0';

    /**
     * 自动全选
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.selectAll = options.selectAll || '0';
    }

    /**
     * 异步树
     * 会签节点没有
     */
    if (['countersign'].indexOf(nodeType) == -1) {
      this.asynTree = options.asynTree || '0';
    }

    /**
     * 可跳跃
     * 会签节点,候选,多人节点没有
     */
    if (['countersign', 'joint', 'batchtask'].indexOf(nodeType) == -1) {
      this.isJump = options.isJump || '0';
    }

    /**
     * 启用逻辑
     * 会签节点,判断,多人节点没有
     */
    if (['countersign', 'decision', 'batchtask'].indexOf(nodeType) == -1) {
      this.logicalJudgment = options.logicalJudgment || '0';
    }

    /**
     * 顺序审批
     * 多人审批节点特有
     */
    if (nodeType == 'batchtask') {
      this.sequential = options.sequential || '0';
    }
    /**
     * 是否开启密级
     */
    this.securityEnable = options.securityEnable || '0';
    /**
     * 密级类型
     */
    this.securityCode = options.securityCode || '';
  }

  getCategorydefinition(nodeType) {
    switch (nodeType) {
      //任务节点
      case 'task':
        return 'kaiteUserTask';
      //判断节点
      case 'decision':
        return 'kaiteDecideUserTask';
    }
  }
}
