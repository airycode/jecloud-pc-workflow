export default class FormExtendConfig {
  constructor(options) {
    /**
     * 可发起
     */
    this.canSponsor = options.canSponsor || '1';

    /**
     * 可取回
     */
    this.canRetrieve = options.canRetrieve || '1';

    /**
     * 可退回
     */
    this.canReturn = options.canReturn || '1';

    /**
     * 可催办
     */
    this.canUrged = options.canUrged || '1';

    /**
     * 可撤销
     */
    this.canCancel = options.canCancel || '1';

    /**
     * 可作废
     */
    this.canInvalid = options.canInvalid || '1';
    /**
     * 可转移
     */
    this.canTransfer = options.canTransfer || '0';
    /**
     * 可委托
     */
    this.canDelegate = options.canDelegate || '0';

    /**
     * 简易发起
     */
    this.easyLaunch = options.easyLaunch || '0';

    /**
     * 简易审批
     */
    this.simpleApproval = options.simpleApproval || '0';

    /**
     * 隐藏状态信息
     */
    this.hideStateInfo = options.hideStateInfo || '0';

    /**
     * 审批耗时信息
     */
    this.hideTimeInfo = options.hideTimeInfo || '0';
  }
}
