export default class NodeFormControl {
  constructor(options) {
    /**
     * 字段赋值
     */
    this.fieldAssignment = options.fieldAssignment || {};

    /**
     * 字段配值
     * {
          "code": {
              "editable 编辑": true,
              "readonly 只读": true,
              "hidden  隐藏": true,
              "display 显示": true,
              "required 必填": true
          }
        }
     */
    this.fieldControl = options.fieldControl || {};

    /**
     * 按钮配置
     * "code": {
				"enable": true
			}
     */
    this.taskFormButton = options.taskFormButton || {};

    /**
     * 子功能
     * {
          "code": {
              "editable": true,
              "hidden": true,
              "display": true
          }
        }
     */
    this.taskChildFunc = options.taskChildFunc || {};
  }
}
