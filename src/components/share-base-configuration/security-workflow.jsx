import { ref } from 'vue';
import { defineComponent } from 'vue';
import { loadSecurityMicro } from '@jecloud/utils';
/**
 * 流程节点密级
 */
export default defineComponent({
  props: { model: Object },
  setup(props) {
    let rendered = ref(false);
    let security = null;
    loadSecurityMicro().then((plugin) => {
      security = plugin;
      rendered.value = !!plugin;
    });

    return () => (rendered.value ? security.renderWorkflowField({ model: props.model }) : null);
  },
});
