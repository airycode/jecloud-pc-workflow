/**
 * API_URL命名规则：API_模块_方法
 */

//通用loadurl
export const APT_FORMBASE_LOAD = '/je/common/load';

//获得角色树的数据
export const API_SELECTPERSONNEL_LOADTREEDATA = '/je/meta/dictionary/tree/loadTree';

//数据保存接口
export const API_WORKFLOW_DOSAVE = '/je/workflow/model/doSave';

//数据保存并发布
export const API_WORKFLOW_UPDATEANDDEPLOY = '/je/workflow/model/updateAndDeploy';

//获得提醒方式的数据
export const API_WORKFLOW_GETMESSAGETYPE = '/je/meta/setting/getMessageType';

//查询单条数据
export const API_WORKFLOW_GETINFOBYID = '/je/workflow/model/getInfoById';
