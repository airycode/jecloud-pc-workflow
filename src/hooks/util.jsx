import { h } from 'vue';
import WorkflowLayout from '../views/layout/index.vue';
import { Modal } from '@jecloud/ui';

export function showWorkFlowWin({ workflowId, funcData }, fn) {
  Modal.window({
    title: '工作流引擎',
    width: '100%',
    height: '100%',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;',
    content() {
      return h(WorkflowLayout, { workflowId, funcData });
    },
    //关闭方法
    onClose(model) {
      fn && fn();
    },
  });
}
